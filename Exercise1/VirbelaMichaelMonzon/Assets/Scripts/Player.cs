using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Added for Exception Handling
using System;

public class Player : MonoBehaviour
{
    //Must know our own position to compare it to other items
    Vector3 m_myPosition;
    //Each Item will send its object name. The position will be retrieved from the stack and used to compare to others in the frame
    Stack<string> m_positionCompares;
    //Keep track of the distance to the closest object to reduce the number of overall comparisons we perform.
    float m_distanceToClosestObject = 0;
    //Keep track of GameObject that is currently red, so that we can deactivate the last one and change this to the new one. Initialized to avoid null references
    //Will be named the current gameobject
    string m_CurrentHighlightedObject = "";
    //Keep track if we need to mark a new frame for tracking purposes
    Boolean m_newFrame = true;

    // Start is called before the first frame update
    void Start()
    {
        UpdatePosition();
        m_positionCompares = new Stack<string>();
    }

    // Update is called once per frame
    void Update()
    {
        //Kept incase we move the player in the editor or through other means
        UpdatePosition();

        CheckPositions();
    }


    //This item has a few phases.  The items will run in this order:
    //1: check if this is the first frame. If it is, we automatically set the new color to the first item in the list to avoid null refs.
    //2: iterate through the stack list so long as it is not empty. Update the closest items in the list.
    private void CheckPositions()
    {
        //Make sure we are not running with an empty list of items. If we're not, begin work
        if (m_positionCompares.Count > 0)
        {
            //exception handling as a failsafe
            try
            {
                //Give it the existing value since we need something to Compare;
                string newClosest = m_CurrentHighlightedObject;
                //This will be used to check the next item in the stack
                string checkme = "";

                //Check each item in the stack and compare it to the current closest object
                while (m_positionCompares.Count > 0)
                {
                    //Check if this is a new frame
                    if (m_newFrame)
                    {
                        Debug.Log("Checking first item color");
                        //Clear any previous data
                        if (m_CurrentHighlightedObject != "") ResetObjectColor(m_CurrentHighlightedObject);
                        checkme = m_positionCompares.Pop();
                        //Automatically set the closest item since this is the first frame and change to the desired color
                        SetNewClosestItem(checkme);
                        ChangeObjectColor(checkme);

                        //mark this as no longer the first loop in the series
                        m_newFrame = false;
                    }

                    else //This is not the first frame iteration
                    {
                        checkme = m_positionCompares.Pop();
                        //Run the sequence to compare the next item in the stack to the currently highlighted object. If true, change color. 
                        if (CompareToClosest(checkme))
                        {
                            Debug.Log("Updating closest item to " + checkme);
                            ResetObjectColor(m_CurrentHighlightedObject);
                            SetNewClosestItem(checkme);
                            ChangeObjectColor(checkme);
                        }
                        else
                        {
                            //Nothing to do, this item is not closer than the last one.
                        }
                    }
                }
                //As we leave the function, mark the frame for a new frame
                m_newFrame = true;
            }
            catch (Exception e)
            {
                Debug.Log("Error performing compare function. Check message:");
                Debug.Log(e);
                //Leave the function and return back to program
                return;
            }
        }
        //Else Do nothing, because we have no objects in the frame
    }
    //Checks a new object compared to the currently highlighted object. Returns true if the new object is clo
    private bool CompareToClosest( string a_newobject )
    {
        if (GetItemDistance(a_newobject) < m_distanceToClosestObject)
        {
            return true;
        }
        else return false;
    }
    private void SetNewClosestItem(string a_objectname)
    {
        m_CurrentHighlightedObject = a_objectname;
        m_distanceToClosestObject = GetItemDistance(a_objectname);
    }

    void UpdatePosition()
    {
        m_myPosition = transform.position;
    }

    public void AddToStack(string name)
    {
        m_positionCompares.Push(name);
    }
    private void ChangeObjectColor(string a_objectname)
    {
        GameObject.Find(a_objectname).GetComponent<Item>().ChangeColor();
    }

    //Resets the object color of the 
    private void ResetObjectColor(string a_objectname)
    {
        GameObject.Find(a_objectname).GetComponent<Item>().ResetColor();
    }

    //Returns the distance between the player and the arg item
    private float GetItemDistance(string a_item)
    {
        GameObject compareitem = GameObject.Find(a_item);        
        return Vector3.Distance(this.m_myPosition, compareitem.transform.position);
    }    
}
