using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class ColorSettings : MonoBehaviour
{
    [SerializeField] public Color BotBaseColor = Color.white;
    [SerializeField] public Color BotHighlightColor = Color.blue;
    [SerializeField] public Color ItemBaseColor = Color.white;
    [SerializeField] public Color ItemHighlightColor = Color.red;
}
