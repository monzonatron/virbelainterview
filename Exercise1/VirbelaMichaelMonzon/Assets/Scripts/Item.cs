using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Vector3 myPosition;
    public Color m_baseColor = Color.white;
    public Color m_highlightColor = Color.red;

    //We will decide the type of item this is here, so it can be viewed in the future
    public enum BotType
    {
        Bot,
        Item
    }
    public BotType m_botType;

    // Start is called before the first frame update
    public virtual void Start()
    {
        //Start by creating this item's position as a Vector (since we are in 3d space)
        myPosition = transform.position;
        m_initialposition = myPosition;
        m_botType = BotType.Item;

        //Update all color settings and set the current base colors
        UpdateColorSettings();
        if (gameObject.GetComponent<Renderer>().material.color != m_baseColor) gameObject.GetComponent<Renderer>().material.color = m_baseColor;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        UpdateColorSettings();
        ArbitraryMovement();
        UpdatePosition();
        SendPositionToPlayer();
        
    }
    public void UpdateColorSettings()
    {
        //Change the base and hgihlight color according to what we are
        if  (this.m_botType == BotType.Bot)
        {
            m_baseColor = GameObject.Find("COLOR_OVERRIDES").GetComponent<ColorSettings>().BotBaseColor;
            m_highlightColor = GameObject.Find("COLOR_OVERRIDES").GetComponent<ColorSettings>().BotHighlightColor;
        }
        else
        {
            m_baseColor = GameObject.Find("COLOR_OVERRIDES").GetComponent<ColorSettings>().ItemBaseColor;
            m_highlightColor = GameObject.Find("COLOR_OVERRIDES").GetComponent<ColorSettings>().ItemHighlightColor;
        }

    }

    /// <summary>
    /// Class created to Update the variable containing this item's position.
    /// </summary>
    public void UpdatePosition()
    {
        myPosition = transform.position;
    }

    //Change color according to what we are
    public void ChangeColor()
    {
        switch (m_botType)
        {
            case BotType.Bot:
                gameObject.GetComponent<Renderer>().material.color = m_highlightColor;
                break;
            case BotType.Item:
                gameObject.GetComponent<Renderer>().material.color = m_highlightColor;
                break;
        }

    }
    public void ResetColor()
    {
        gameObject.GetComponent<Renderer>().material.color = m_baseColor;
    }

    public void SendPositionToPlayer()
    {
        //Pass this name to the player object for comparison sorting
        (GameObject.FindObjectOfType(typeof(Player)) as Player).AddToStack(this.name);
    }

    //MovementSpeed
    public float m_movementSpeed = .005f;
    Vector3 m_initialposition;

    //Replaced a bool statement to use as the new direction. Wanted it to be more user-readable.
    public  Direction m_myDirection = Direction.left;

    public enum Direction
    {
        left,
        right,
        up,
        down
    }

    public void ArbitraryMovement()
    {
        switch (m_myDirection)
        {
            case Direction.left:
                myPosition.x = myPosition.x - m_movementSpeed;
                break;
            case Direction.right:
                myPosition.x = myPosition.x + m_movementSpeed;
                break;
            case Direction.up:
                myPosition.y = myPosition.y - m_movementSpeed;
                break;
            case Direction.down:
                myPosition.y = myPosition.y + m_movementSpeed;
                break;
        }

        //Update object Position
        transform.position = myPosition;

        //Check if we need to flip the movement direction
        if (Vector3.Distance(myPosition, m_initialposition) > 10f) FlipMovement();
    }

    private void FlipMovement()
    {
        switch (m_myDirection)
        {
            case Direction.left:
                m_myDirection = Direction.right;
                break;
            case Direction.right:
                m_myDirection = Direction.left;
                break;
            case Direction.up:
                m_myDirection = Direction.down;
                break;
            case Direction.down:
                m_myDirection = Direction.up;
                break;
        }

    }
}
