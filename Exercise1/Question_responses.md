Questions

    How can your implementation be optimized?

* Right now, the function in player which does the update per frame sacrifices a little bit of speed for readability. The compare function for calculating distance is called twice per frame instead of once, which is more taxing than a simpler solution. I also wanted to experiment with some sort of asynchronous timing between the player and the location of all of the objects, so the objects were no longer dependent on a frame by frame update to perform the distance calculation function.

* Considering the data structure used for this test, it may have been more optimizable to add all of the Objects (items/bots) into a LinkedList using a keyvalue pair rather than using a stack for each frame's comparing function.

    How much time did you spend on your implementation?

* About Seven hours in total. Five hours was used for the functional implementations, afterwards two hours was spent refactoring the code (the player class was a bit of a mess and needed a bit of re-work).


    What was most challenging for you?

* Figuring out an implementation for the Color picker at editor time. I'd seen solutions proposed by Wes during some devlearns and initially implemented a menu item in the toolbar for changing the colors, but ultimately backtracked and changed the control to a unity object color picker. I feel the first solution was much more elegant, but I was already encroaching on 7 hours and wanted to implement something more quickly.

    What else would you add to this exercise?

* I feel as though the test was targeting the following skillsets:

- Phase 1: Object Oriented Problem Solving
- Phase 2: Polymorphism and inheritence
- Phase 3: Scalability 
- Phase 4: Developer Tool Implementation

The optional tests are good for Dev-side implementaion testing, Serialization practice, Search Algorithm or Data Structure Optimization.

There are no items targetting a lot of what is used in Virbela, such as UI design, pathfinding or additional technology implementation. Having the user use a foreign class as a starting point and asking to implement it (such as a 3rd object type with obscured code or acting as a library) could be an applicable challenge. 

Or, while this may be way too much complexity for a short coding challenge, create a local server which acts as the function for changing the closest object to the player red.

