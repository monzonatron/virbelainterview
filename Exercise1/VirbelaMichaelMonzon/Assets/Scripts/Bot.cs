using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : Item
{
    // Start is called before the first frame update
    override public void Start()
    {
        myPosition = transform.position;
        m_myDirection = Direction.up;
        m_botType = BotType.Bot;
        m_baseColor = Color.white;
        m_highlightColor = Color.blue;

        UpdateColorSettings();
        if (gameObject.GetComponent<Renderer>().material.color != m_baseColor) gameObject.GetComponent<Renderer>().material.color = m_baseColor;


    }

    // Update is called once per frame
    override public void Update()
    {
        UpdateColorSettings();
        ArbitraryMovement();
        UpdatePosition();
        SendPositionToPlayer();
    }
}
